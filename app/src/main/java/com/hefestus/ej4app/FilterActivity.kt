package com.hefestus.ej4app

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_filter.*

class FilterActivity : AppCompatActivity() {
    var filter = EJ4App.get().source.getFilter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_filter)
        initFilters()

        btn_back.setOnClickListener { finish() }
        btn_clean.setOnClickListener {
            filter = Filter()
            initFilters()
        }
        btn_apply.setOnClickListener {
            EJ4App.get().source.setFilter(filter)
            finish()
        }

        day_1.setOnClickListener {
            filter.days[0] = !filter.days[0]
            day_1.selectChip(filter.days[0])
        }

        day_2.setOnClickListener {
            filter.days[1] = !filter.days[1]
            day_2.selectChip(filter.days[1])
        }

        day_3.setOnClickListener {
            filter.days[2] = !filter.days[2]
            day_3.selectChip(filter.days[2])
        }

        day_4.setOnClickListener {
            filter.days[3] = !filter.days[3]
            day_4.selectChip(filter.days[3])
        }

        day_5.setOnClickListener {
            filter.days[4] = !filter.days[4]
            day_5.selectChip(filter.days[4])
        }

        day_6.setOnClickListener {
            filter.days[5] = !filter.days[5]
            day_6.selectChip(filter.days[5])
        }

        day_7.setOnClickListener {
            filter.days[6] = !filter.days[6]
            day_7.selectChip(filter.days[6])
        }

        period_1.setOnClickListener {
            filter.periods[0] = !filter.periods[0]
            period_1.selectChip(filter.periods[0])
        }

        period_2.setOnClickListener {
            filter.periods[1] = !filter.periods[1]
            period_2.selectChip(filter.periods[1])
        }

        period_3.setOnClickListener {
            filter.periods[2] = !filter.periods[2]
            period_3.selectChip(filter.periods[2])
        }
    }

    private fun initFilters() {
        day_1.selectChip(filter.days[0])
        day_2.selectChip(filter.days[1])
        day_3.selectChip(filter.days[2])
        day_4.selectChip(filter.days[3])
        day_5.selectChip(filter.days[4])
        day_6.selectChip(filter.days[5])
        day_7.selectChip(filter.days[6])
        period_1.selectChip(filter.periods[0])
        period_2.selectChip(filter.periods[1])
        period_3.selectChip(filter.periods[2])

    }
}

