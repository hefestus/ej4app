package com.hefestus.ej4app

import com.google.android.gms.maps.model.LatLng
import io.reactivex.Flowable

class StayPointSource(private var filter: Filter = Filter()) {
    fun getStayPoints(): Flowable<StayPoint> =
            mockStayPoints()
                    .map { stayPoint -> stayPoint.copy(hits = stayPoint.hits.filter { it.isAllowed() })}
                    .filter { it.hits.isNotEmpty() }

    fun getFilter(): Filter = filter.copy()
    fun setFilter(newFilter: Filter) {
        filter = newFilter
    }

    private fun Hit.isAllowed(): Boolean =
            !filter.isOn || (filter.dayAllowed(weekDay) && filter.periodAllowed(time))
}

data class StayPoint(val id: String,
                     val position: LatLng,
                     val state: String,
                     val hits: List<Hit>)

data class Hit(val id: String,
               val day: String,
               val weekDay: String,
               val time: String)

class Filter {
    var isOn: Boolean = false
    val days: BooleanArray = booleanArrayOf(false, false, false, false, false, false, false)
    val periods: BooleanArray = booleanArrayOf(false, false, false, false)
    fun copy(): Filter {
        val newFilter = Filter()
        days.forEachIndexed { ind, value ->
            newFilter.days[ind] = value
            isOn = isOn || value
        }
        periods.forEachIndexed { ind, value ->
            newFilter.periods[ind] = value
            isOn = isOn || value
        }
        return newFilter
    }

    fun dayAllowed(day: String): Boolean = when(day) {
        "MONDAY" -> days[0]
        "TUESDAY" -> days[1]
        "WEDNESDAY" -> days[2]
        "THURSDAY" -> days[3]
        "FRIDAY" -> days[4]
        "SATURDAY" -> days[5]
        "SUNDAY" -> days[6]
        else -> false
    }

    fun periodAllowed(period: String): Boolean = when(period.substring(0,2).asInt()) {
        in 0..12 -> periods[0]
        in 13..18 -> periods[1]
        in 19..23 -> periods[2]
        else -> false
    }
}

fun mockStayPoints(): Flowable<StayPoint>  = Flowable.just(
        StayPoint("sp1", LatLng(-21.68233,-51.07765), "SP", listOf(fixHit1)),
        StayPoint("sp2", LatLng(-21.68103,-51.08392), "SP", listOf(fixHit1, fixHit2)),
        StayPoint("sp3", LatLng(-21.69253,-51.06532), "SP", listOf(fixHit1, fixHit3)),
        StayPoint("sp4", LatLng(-21.68325,-51.08225), "SP", listOf(fixHit1, fixHit2, fixHit3)),
        StayPoint("sp5", LatLng(-21.68683,-51.07293), "SP", listOf(fixHit2, fixHit3))
)

val fixHit1 = Hit("h1", "1", "MONDAY", "11:58:18")
val fixHit2 = Hit("h2", "1", "TUESDAY", "15:58:18")
val fixHit3 = Hit("h3", "1", "TUESDAY", "21:58:18")