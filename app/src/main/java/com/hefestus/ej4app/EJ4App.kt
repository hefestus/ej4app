package com.hefestus.ej4app

import android.app.Application
import timber.log.Timber

class EJ4App: Application() {
    val source: StayPointSource = StayPointSource()

    companion object {
        lateinit private var INSTANCE: EJ4App
        fun get(): EJ4App = INSTANCE
    }

    override fun onCreate() {
        super.onCreate()
        INSTANCE = this
        Timber.plant(Timber.DebugTree())
    }
}