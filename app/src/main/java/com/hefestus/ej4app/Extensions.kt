package com.hefestus.ej4app

import android.app.Activity
import android.content.Context
import android.os.IBinder
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.Subject

val ioScheduler: Scheduler get() = Schedulers.io()
val uiScheduler: Scheduler get() = AndroidSchedulers.mainThread()

infix fun <T> Subject<T>.send(emission: T) = this.onNext(emission)

operator fun CompositeDisposable.minusAssign(d: Disposable) { this.remove(d) }
operator fun CompositeDisposable.plusAssign(d: Disposable) { this.add(d) }

infix fun ViewGroup?.inflate(layoutId: Int): View =
        this?.let { LayoutInflater.from(context).inflate(layoutId, this, false) }
                ?: throw IllegalStateException("Cannot inflate layout on null a view group")

fun View?.hideKeyboard() = this?.let { context.hideKeyboard(windowToken) }

fun Activity?.hideKeyboard() = this?.let { hideKeyboard(currentFocus?.windowToken) }

fun Context.hideKeyboard(windowToken: IBinder?) = windowToken?.let {
    val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as? InputMethodManager
    imm?.hideSoftInputFromWindow(windowToken, 0)
}

fun TextView.selectChip(selected: Boolean) =
        if (selected) {
            setBackgroundResource(R.drawable.bg_chip_selected)
            setTextColor(resources.getColor(R.color.white))
        } else {
            setBackgroundResource(R.drawable.bg_chip)
            setTextColor(resources.getColor(R.color.colorPrimary))
        }

fun String?.asInt(): Int =
        try { orEmpty().toInt() }
        catch (e: Exception) { -1 }