package com.hefestus.ej4app

import android.location.Address
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject

class AddressesAdapter(private val addressClicks: PublishSubject<Address> = PublishSubject.create(),
                       private val viewModels: MutableList<Address> = ArrayList(3)): RecyclerView.Adapter<AddressesViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): AddressesViewHolder {
        return AddressesViewHolder(parent.inflate(R.layout.item_address))
    }

    override fun onBindViewHolder(holder: AddressesViewHolder?, position: Int) {
        holder?.bind(viewModels[position], addressClicks)
    }

    override fun getItemViewType(position: Int) = position

    override fun getItemCount() = viewModels.size

    fun clearModels() {
        viewModels.clear()
        notifyDataSetChanged()
    }

    fun addModels(models: List<Address>) {
        viewModels.clear()
        viewModels.addAll(models)
        notifyDataSetChanged()
    }

    fun addressClicks(): Observable<Address> = addressClicks
}

class AddressesViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
    private val name: TextView = itemView.findViewById(R.id.label_address)

    fun bind(model: Address, addressClicks: PublishSubject<Address>) {
        name.text = model.getAddressLine(0).orEmpty()
        name.setOnClickListener { addressClicks send model }
    }
}