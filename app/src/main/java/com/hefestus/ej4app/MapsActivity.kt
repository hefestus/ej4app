package com.hefestus.ej4app

import android.location.Address
import android.location.Location
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.jakewharton.rxbinding2.widget.RxTextView
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.disposables.Disposables
import kotlinx.android.synthetic.main.activity_map.*
import org.jetbrains.anko.startActivity
import pl.charmas.android.reactivelocation2.ReactiveLocationProvider
import timber.log.Timber
import java.util.concurrent.TimeUnit

class MapsActivity: AppCompatActivity(), OnMapReadyCallback {
    private val addressAdapter: AddressesAdapter = AddressesAdapter()

    private var googleMap: GoogleMap? = null
    private lateinit var mapFragment: SupportMapFragment
    private lateinit var locationProvider: ReactiveLocationProvider
    private var locationSub: Disposable = Disposables.disposed()
    private var pointsSub: Disposable = Disposables.disposed()
    private var subs = CompositeDisposable()
    private var position: LatLng = LatLng(-34.0, 151.0)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_map)
        locationProvider = ReactiveLocationProvider(this)
        locationSub = locationProvider.lastKnownLocation
                .observeOn(uiScheduler)
                .subscribeOn(ioScheduler)
                .subscribe({ it.setPosition() }, { locationProviderError(it) })
        mapFragment = SupportMapFragment.newInstance(GoogleMapOptions()
                                                        .rotateGesturesEnabled(false)
                                                        .tiltGesturesEnabled(false)
                                                        .compassEnabled(false)
                                                        .mapToolbarEnabled(false))
        supportFragmentManager.beginTransaction()
                .add(R.id.map_fragment, mapFragment)
                .commit()
        mapFragment.getMapAsync(this)

        btn_location.setOnClickListener { locationClicked() }
        btn_reload.setOnClickListener { reloadClicked() }
        btn_filter.setOnClickListener { filterClicked() }

        rv_addresses.layoutManager = LinearLayoutManager(this)
        rv_addresses.adapter = addressAdapter

        subs.clear()
        subs += addressAdapter.addressClicks()
                .observeOn(uiScheduler)
                .subscribe { addressSelected(it) }
        subs += RxTextView.textChanges(edit_search)
                .filter { it.isNotEmpty() }
                .throttleLast(1000, TimeUnit.MILLISECONDS)
                .observeOn(uiScheduler)
                .subscribe { geocode(it.toString()) }
        hideKeyboard()
    }

    override fun onDestroy() {
        subs.clear()
        locationSub.dispose()
        super.onDestroy()
    }

    override fun onMapReady(googleMap: GoogleMap) {
        this.googleMap = googleMap
        this.googleMap?.run {
            setOnCameraIdleListener { onCameraIdle() }
            setOnMarkerClickListener { onMarkerClick(it) }
            setMyLocationEnabled(false)
        }
    }

    private fun onCameraIdle() {
        Timber.d("onCameraIdle")
    }

    private fun onMarkerClick(marker: Marker): Boolean {
        (marker.tag as? StayPoint).let { Timber.d("onMarkerClick: ${it?.id}") }
        startActivity<DetailsActivity>()
        return true
    }

    private fun locationClicked() {
        Timber.d("locationClicked")
        locationSub.dispose()
        locationSub = locationProvider.getUpdatedLocation(LocationRequest.create()
                                      .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                                      .setNumUpdates(1))
                .observeOn(uiScheduler)
                .subscribeOn(ioScheduler)
                .subscribe({ it.setPosition() }, { locationProviderError(it) })
    }

    private fun reloadClicked() {
        googleMap?.clear()
        pointsSub.dispose()
        pointsSub = EJ4App.get().source.getStayPoints()
                .observeOn(uiScheduler)
                .subscribeOn(ioScheduler)
                .subscribe({ plot(it) }, { Timber.e("getStayPoints: $it") }, { refreshPosition() })
    }

    private fun filterClicked() {
        startActivity<FilterActivity>()
    }

    private fun geocode(address: String) {
        locationSub.dispose()
        locationSub = locationProvider.getGeocodeObservable(address, 3)
                .observeOn(uiScheduler)
                .subscribeOn(ioScheduler)
                .subscribe({ addressAdapter.addModels(it) }, { locationProviderError(it) })
    }

    private fun addressSelected(address: Address) {
        hideKeyboard()
        position = LatLng(address.latitude, address.longitude)
        edit_search.setText("")
        addressAdapter.clearModels()
        refreshPosition()
    }

    private fun Location.setPosition() {
        position = LatLng(latitude, longitude)
        refreshPosition()
    }

    private fun refreshPosition() {
        googleMap?.moveCamera(CameraUpdateFactory.newLatLngZoom(position, 14f))
    }

    private fun plot(stayPoint: StayPoint) {
        position = stayPoint.position
        googleMap?.addMarker(MarkerOptions().position(position))?.tag = stayPoint
    }

    private fun locationProviderError(thowable: Throwable) {
        Timber.e("locationProviderError: ${thowable.message}")
    }
}

